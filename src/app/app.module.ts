import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CommonUtils } from '../common/commonUtils';



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CustomSplashPage } from "../pages/custom-splash/custom-splash";
import { LoginPage } from "../pages/login/login";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DummyHomePage } from "../pages/dummy-home/dummy-home";
import { ApiServiceProvider } from '../providers/api-service/api-service';

import { HttpModule } from '@angular/http';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { DatePicker } from '@ionic-native/date-picker';
import { TabContentPage } from "../pages/tab-content/tab-content";
import { DepartmentAndDoctorsPage } from "../pages/department-and-doctors/department-and-doctors";
import { LapReportsPage } from "../pages/lap-reports/lap-reports";
import { DoctorSearchPipe } from '../pipes/doctorSearchPipe';
import { DoctorDetailsPage } from '../pages/doctor-details/doctor-details';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import { PharmecySearchmedPipe } from '../pipes/pharmecySearchmedPipe';
import { PharmacyListPage } from "../pages/pharmacy/pharmacyList";
import { AppointmentListPage } from '../pages/appointment-list/appointment-list';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CustomSplashPage,
    LoginPage,
    DummyHomePage,
    TabContentPage,
    DepartmentAndDoctorsPage,
    LapReportsPage,
    DoctorSearchPipe,
    PharmecySearchmedPipe,
    DoctorDetailsPage,
    AppointmentListPage,
    PharmacyListPage
   
  ],
  imports: [
    BrowserModule,HttpModule,
    BrowserAnimationsModule,
    
    IonicModule.forRoot(MyApp,{tabsHideOnSubPages: true}),
  ],
  bootstrap: [IonicApp],
  entryComponents: [  
    MyApp,
    HomePage,
    ListPage,
    CustomSplashPage,
    LoginPage,
    DummyHomePage,
    TabContentPage,
    DepartmentAndDoctorsPage,
    LapReportsPage,
    DoctorDetailsPage,
    AppointmentListPage,
    PharmacyListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    SocialSharing,
    NativePageTransitions,
    CommonUtils,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,DoctorSearchPipe,DatePicker,PharmecySearchmedPipe
  ]
})
export class AppModule {}
