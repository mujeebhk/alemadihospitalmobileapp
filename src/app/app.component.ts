import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { CustomSplashPage } from "../pages/custom-splash/custom-splash";
import { LoginPage } from "../pages/login/login";
import { DummyHomePage } from "../pages/dummy-home/dummy-home";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = HomePage;
  //rootPage: any = LoginPage;
  rootPage: any = DummyHomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public modalCtrl: ModalController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      //this.splashScreen.hide();
      //checking if the device is android
      if (this.platform.is('android')) {
        // This will only print when on iOS
        console.log('I am an android device!');
        sessionStorage.setItem('isAndroid','true');
      }else{
        sessionStorage.setItem('isAndroid','false');
        console.log('I am not on android device');
      }

      let splash = this.modalCtrl.create(CustomSplashPage);
       splash.present();
    });
  }

  openPage(page) {
  
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
