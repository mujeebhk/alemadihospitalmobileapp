import { Component } from '@angular/core';
import { ViewController, App } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { LoginPage } from "../login/login";
import { HomePage } from "../home/home";
import { TabContentPage } from "../tab-content/tab-content";

/**
 * Generated class for the CustomSplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-custom-splash',
  templateUrl: 'custom-splash.html',
})
export class CustomSplashPage {

  constructor(public viewCtrl: ViewController,public splashScreen: SplashScreen,public app: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomSplashPage');
  }

  ionViewDidEnter() {
    
       this.splashScreen.hide();
    
       setTimeout(() => {
         this.viewCtrl.dismiss();
       this.app.getRootNav().setRoot( LoginPage );
       //this.app.getRootNav().setRoot( TabContentPage );
       },2500);
    
  }
}
