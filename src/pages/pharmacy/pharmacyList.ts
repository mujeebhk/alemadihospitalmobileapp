import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

/**
 * Generated class for the pharmacyList page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare global {
  interface Array<T> {
    remove(elem: T): Array<T>;
  }
}

@Component({
  selector: 'pharmacyList',
  templateUrl: 'pharmacyList.html',
})
export class PharmacyListPage {
    showPharmacySearchUI: boolean=false;

  public pharmacyList=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public apiServiceProvider:ApiServiceProvider,public loadingCtrl: LoadingController) {
  
    
      setTimeout(()=>{
          console.log('firing pharmecy api');
          this.getPharmacyDataFromTheServer();
      },500);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LapReportsPage');
  }

  getPharmacyDataFromTheServer(){
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();
    this.apiServiceProvider.getPharmacyList().subscribe((data)=>{
      this.pharmacyList=data;
      console.log('getting pharmact data from the server using  ',this.pharmacyList)
      console.log('sorting pharmacy data albaticaly using ')
      this.pharmacyList.sort(this.compare);
      console.log('sorted arrays is',this.pharmacyList);
      loading.dismiss();
    },(error)=>{
      console.log('error while fetching the list from the server',error);
      loading.dismiss();
    });

    
    // this.apiServiceProvider.getDummyJsonDataFromSavedFile('pharmacy.json').subscribe((data)=>{
    //   this.pharmacyList=data;
    //   console.log('getting pharmact data from the server using  ',this.pharmacyList)
    //   console.log('sorting pharmacy data albaticaly using ')
    //   this.pharmacyList.sort(this.compare);
    //   console.log('sorted arrays is',this.pharmacyList);
    //   loading.dismiss();
    // },(error)=>{
    //   console.log('error while fetching the list from the server',error);
    //   loading.dismiss();
    // });


  }

  toggleSection(i) {
    this.pharmacyList[i].open = !this.pharmacyList[i].open;
  }

  toggleSearchInputBox(){
    this.showPharmacySearchUI=this.showPharmacySearchUI===true?false:true;
    console.log(this.showPharmacySearchUI);
  }

 
  toggleItem(i, j) {
    this.pharmacyList[i].test[j].open = !this.pharmacyList[i].test[j].open;
  }


  myHeaderFn(record, recordIndex, records) {
    if(recordIndex!==0&&records[recordIndex].name.charAt(0)>records[recordIndex-1].name.charAt(0)){
      return record.name.charAt(0);
    }
    return null;
  }

  getAvailbility(medDetalis){
   // console.log('have to return class name',medDetalis);
   if(medDetalis.quantity>0)
   return 'medAvailableText';
   else
   return 'medUnAvailableText'
  }

  compare(a,b) {
    if (a.name < b.name)
      return -1;
    if (a.name > b.name)
      return 1;
    return 0;
  }  

}
