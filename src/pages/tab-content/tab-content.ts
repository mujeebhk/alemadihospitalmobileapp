import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";

/**
 * Generated class for the TabContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: "page-tab-content",
  templateUrl: "tab-content.html"
})
export class TabContentPage {
  tab4: any;
  tab3: any;
  tab2: any;
  tab1: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tab1 = HomePage;
    this.tab2 = HomePage;
    this.tab3 = HomePage;
    this.tab4 = HomePage;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad TabContentPage");
  }
}
