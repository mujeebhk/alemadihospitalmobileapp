import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ToastController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the DoctorDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-doctor-details',
  templateUrl: 'doctor-details.html',
})
export class DoctorDetailsPage {
  noSlotsForDoctor: boolean;
  doctorDetails: any;
  startDate:any;
  doctorsSlots:any;
  public clinicCode:string;

  constructor(public navCtrl: NavController,public alertCtrl: AlertController,public toastCtrl: ToastController, public navParams: NavParams,public datePicker: DatePicker,public apiServiceProvider:ApiServiceProvider,public loadingCtrl: LoadingController) {
    this.doctorDetails=this.navParams.get('doctorDetails');
    console.log(this.doctorDetails);
    this.clinicCode=this.doctorDetails.clinicCode.toString().trim();  
  }


  parseDate(dateString: string): Date {
    if (dateString) {
      console.log(dateString);
        return new Date(dateString);
    } else {
        return null;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorDetailsPage');
  }
  dateChanged(event){
    this.startDate = event
    let dt = new Date(event);
    console.log(dt);
    console.log(this.startDate);
    if(event!==null||event!==undefined){
      this.getSlotsForDoctor(dt);
    }
  }

  getSlotsForDoctor(dateObj){
    
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();
    this.apiServiceProvider.getSlotsDetailsForDoctor(dateObj,this.clinicCode).subscribe((data)=>{
      console.log(data);
      loading.dismiss();
      this.noSlotsForDoctor=false;
      this.doctorsSlots=data;
    },(error)=>{
      loading.dismiss();
      this.noSlotsForDoctor=true;
      console.log(error);
    });
  }
  setSlot(selectedSlot){
    console.log("selected slot",)
    if(selectedSlot.availableStatus || selectedSlot.availableStatus==true){
      this.slotAlert(selectedSlot);
    }
  }

  slotAlert(selectedSlot){
    let alert = this.alertCtrl.create({
      title: 'Confirm appointment',
      message: 'Do you want to book appointment with Dr.'+ this.doctorDetails.name+' on ' + this.startDate +' at '+selectedSlot.slotTime+' ?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.saveAppointmentTodb(selectedSlot);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

  saveAppointmentTodb(selectedSlot){
    let userDetails;
    let patcode;
    let date =new Date(this.startDate+" "+selectedSlot.slotTime)
    console.log(date);
    let userDetailStr=sessionStorage.getItem("userDetails");
    if(userDetailStr && userDetailStr !=''){
      userDetails=JSON.parse(userDetailStr);
      patcode=userDetails.PATCODE;
    }
    let data=
    {
    //  "TRANSNO" :55891,
     "patcode": patcode,
     "CLINICCODE" : parseInt(this.clinicCode),
     "appointmentDate": date
    }
    console.log(data);
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();
    this.apiServiceProvider.saveAppointment(data).subscribe((data)=>{
      console.log(data);
      loading.dismiss(); 
      this.dateChanged(this.startDate);
      this.showToastMsg("Appointment Booked Successfuly.")   
    },(error)=>{
      loading.dismiss();
      this.showToastMsg("Something went wrong, try again.")
      console.log(error);
    });
  }
  showToastMsg(message){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 300,
      position: 'top'
    });
    toast.present();
    return;
  }
}
