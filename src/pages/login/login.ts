import { Component , trigger, state, style, transition, animate, keyframes,ChangeDetectorRef} from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, LoadingController } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";
import {TimerObservable} from "rxjs/observable/TimerObservable";
import {Observable} from 'rxjs/Rx';
import { HomePage } from "../home/home";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { TabContentPage } from "../tab-content/tab-content";


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var window: any;
//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

  animations: [
 
    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('1000ms ease-in-out')
      ])
    ]),
 
    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),
 
    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('1000ms 300ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ])
    ]),
 
    //For send otp button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void <=> *', [
        style({opacity: 0}),
        animate('800ms 1200ms ease-in')
      ])
    ]),

     //For otp input box
     trigger('fadeInOtp', [
      state('in', style({
        opacity: 1
      })), state('out', style({
        opacity: 0
      })),
      transition('out <=> in', [
        style({opacity: 0}),
        animate('600ms 200ms ease-in')
      ])
    ]),
      //For login button
      trigger('rotateLoginButton', [
        state('initial', style({
          opacity: 0.5
        })), state('final', style({backgroundColor: 'green',
          opacity: 1,transform: 'rotate(360deg)'
        })),
        transition('initial <=> final', [
          style({opacity: 1}),
          animate('600ms 200ms ease-in')
        ])
      ])
  ]
})
export class LoginPage {
  public smses:any;

  //variables for animations state
  logoState: any = "in";
  cloudState: any = "in";
  loginState: any = "in";
  formState: any = "in";
  otpState:any="out";
  quatarId:any;
  otpRoundButtonState:any="initial"

  //variables for scripting
  showOtpBox:boolean=false;
  showWaitingForOtpLoadingAnimation:boolean=false;
  showAutomaticallyLoginString:boolean=false;
  private tick: number=6;
  private subscription: Subscription;

  public otpFromAlEmadi;
  public patCode;
  public isAndroid:boolean;
  public userObject={PATIDNUM:null,PATCODE:null,PHOCELL:null,DISNAME:null,POLICYNO:null,LASTVISITED:null,GENDER:null}

  constructor(public navCtrl: NavController, public navParams: NavParams,private chRef: ChangeDetectorRef,public toastCtrl: ToastController,public apiService:ApiServiceProvider,public loadingCtrl: LoadingController ) {
    let isAndroidString:string=sessionStorage.getItem('isAndroid');
    if(isAndroidString==='true'){
      this.isAndroid=true;
    }
    else{
      this.isAndroid=false;
    }

    this.otpFromAlEmadi;
    let that=this;

    //below will exexute only after start watch has been called and a message has arrived
    document.addEventListener('onSMSArrive', function(e:any){
      var data = e.data;
      console.log('SMS arrived, count: ' + data);
      console.log('SMS arrived, count: ' + JSON.stringify(data));
      
      let isValidSms:boolean=that.ifSmsIsFromAlemadi(data);
      if(isValidSms){
        if(that.isAndroid){
          that.stopWatch();
        }
        let otp:string=that.getOtpFromBody(data.body);
       
        if(otp){
          that.animationAfterOtpArrived();
          console.log('got the opt',otp);
          let parsedOtp=parseInt(otp);
          console.log('type of parsed otp is ',typeof(parsedOtp),'and the otp is ',parsedOtp);
          that.otpFromAlEmadi=parsedOtp;
          chRef.detectChanges(); //Whenever you need to force update view
          //once the otp has detected invoke the following animation
         

        }
        else{
          console.log('some error');
        }

      }



    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  SMS(){
    if(window.SMS) window.SMS.listSMS({},data=>{
        setTimeout(()=>{
            console.log(data);
            this.smses=data;
        },0)

    },error=>{
      console.log(error);
    });
  }

  //////submitting the quatar id for getting otp on registered mobile number
  getOtpFromTheServer(){  
    if(this.quatarId===undefined||this.quatarId===null||this.quatarId.length===0){
      const toast = this.toastCtrl.create({
        message: 'Provide a valid Qatar ID',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return;
    }

    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();


    this.apiService.sendOTPusingQatarId(this.quatarId).subscribe((data)=>{
      loading.dismiss();
      console.log('otp sent ',data);
      if(data.length==0){
        console.log('object is empty hence the otp is not sent');
        this.showErrorMessageForInvalidQatarId();
      }else{
        this.patCode=data.PATCODE;
        this.patCode=this.patCode.toString().trim();
        this.showOtpBox=true;
        this.showWaitingForOtpLoadingAnimation=true;
        this.otpState="out";
        this.otpState="in";

        //at this point otp is sent successfully now its time to strat watch for the otp so
        //autodetection feature only for android
        if(this.isAndroid){
          console.log('watch Started for message');
          this.startWatch();
        }
        //now save the data in session Storage
        this.setUserDataInUserObject(data);
        this.saveUserDataToLocalStorage()
       
        console.log('printing user object from inside sendotp ',JSON.stringify(this.userObject));
        //
      }

    },(error)=>{
      loading.dismiss();
      console.log(error);
      console.log('error occured while sending otp');
    },()=>{

    })
  
    console.log(this.showOtpBox);
  }


  setUserDataInUserObject(data){
    this.userObject.DISNAME=data.DISNAME.toString().trim();
    this.userObject.PATCODE=data.PATCODE;
    this.userObject.PATIDNUM=data.PATIDNUM.toString().trim();
    this.userObject.PHOCELL=data.PHOCELL.toString().trim();
    this.userObject.POLICYNO=data.POLICYNO.toString().trim();
    this.userObject.LASTVISITED=data['LAST VISITED'].trim();
    this.userObject.GENDER=data.GENDER.trim();
    
  }

  saveUserDataToLocalStorage(){
    sessionStorage.setItem('userDetails',JSON.stringify(this.userObject))
  }


 

  //////otp detection and population block
  startWatch() {
    if(window.SMS) window.SMS.startWatch(function(){
     console.log('watch started');
    }, function(){
      console.log('failed to start watching');
    });
  }

  stopWatch() {
    if(window.SMS) window.SMS.stopWatch(function(){
      console.log('watching', 'watching stopped');
    }, function(){
      console.log('failed to stop watching');
    });
  }

  //for verfying if the message is from elemadi
  ifSmsIsFromAlemadi(data){
    console.log('verifying if the message is from alemadi ..inside the ifSmsIsFromAlemadi function ',data.address);
    if(data.address==="VK-MBAlts"||data.address==="VM-MBAlts")
    return true;
    else
    false;
  }

  //get opt from message body
  getOtpFromBody(messageBody:string){
    console.log('inside the getOtpFromBody function with messge body',messageBody);
    let otp:string= messageBody.slice(26);
    console.log('inside the getOtpFromBody function with sliced message body',otp);
      if(otp.length!=0){
        otp=otp.trim();
        return otp;
      }
      else{
        return undefined;
      }
  }

  validateOtpAndLogin(){
    if(this.otpFromAlEmadi==undefined||this.otpFromAlEmadi==null){
      this.showErrorMessageForInvalidOtp();
      console.log('empty otp falling back');
      return; 
    }

    this.otpFromAlEmadi=parseInt(this.otpFromAlEmadi);
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();

    this.apiService.verifyOtpAndLogin(this.patCode,this.otpFromAlEmadi).subscribe((data)=>{
      loading.dismiss();
      if(data==true||data=='true'){
        console.log('logged in successfully');
        console.log(data);
        if(this.isAndroid){
          this.stopWatch();
        }
        this.navCtrl.setRoot(TabContentPage);
      }
      else{
        this.showErrorMessageForInvalidOtp();
        return;
      }
      
    },(error)=>{
      loading.dismiss();
      console.log('error occured ',error);
      this.showErrorMessageForInvalidOtp();
    },()=>{

    });
    
    // this.navCtrl.setRoot(HomePage);
  }

  //animation function once the otp has been arrived
  animationAfterOtpArrived(){


    console.log('timeout occured firing animation now');
    this.otpRoundButtonState="final";
    this.showWaitingForOtpLoadingAnimation=false;
    this.showAutomaticallyLoginString=true;

    //also start the timer for automatically login 
    let timer = TimerObservable.create(0, 1000);
    console.log('debugging tick');
    this.subscription = timer.subscribe(t => {
      this.tick = this.tick-1;
      this.chRef.detectChanges();
      console.log('tick is ',this.tick);
        if(this.tick==0){
          this.subscription.unsubscribe();
          this.validateOtpAndLogin();
        }
    });
  }

  showErrorMessageForInvalidQatarId(){
    const toast = this.toastCtrl.create({
      message: 'Invalid Qatar ID Provided',
      duration: 3000,
      position: 'top'
    });
    toast.present();
    return;
  }
  showErrorMessageForInvalidOtp(){
    const toast = this.toastCtrl.create({
      message: 'Invalid Otp Please Try Again',
      duration: 3000,
      position: 'top'
    });
    toast.present();
    return;
  }
  

}
