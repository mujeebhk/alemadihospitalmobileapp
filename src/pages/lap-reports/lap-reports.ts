import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { CommonModule } from '@angular/common';
import { CommonUtils } from '../../common/commonUtils';
import { TabContentPage } from '../tab-content/tab-content';

/**
 * Generated class for the LapReportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-lap-reports',
  templateUrl: 'lap-reports.html',
})
export class LapReportsPage {

  public labReportsList;

  constructor(public commonUtills:CommonUtils,public navCtrl: NavController, public navParams: NavParams,public apiServiceProvider:ApiServiceProvider ,public loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LapReportsPage');
    let userDetails:any=sessionStorage.getItem('userDetails');
    userDetails=JSON.parse(userDetails);
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();
    this.apiServiceProvider.getLabDataUsingPatCode(userDetails.PATCODE).subscribe((data)=>{
      loading.dismiss();
      this.labReportsList=data.reports;
      console.log('getting lab data from the server using patCode ',userDetails.PATCODE,' ',this.labReportsList)
    },(error)=>{  
      console.log(error);
      loading.dismiss();
    });
    
    // this.apiServiceProvider.getDummyJsonDataFromSavedFile('labReports.json').subscribe((data)=>{
    //   this.labReportsList=data.reports;
    //   console.log('getting lab data from the server using patCode ',this.labReportsList);
    //   loading.dismiss();
    // });
  }

  toggleSection(i) {
    this.labReportsList[i].open = !this.labReportsList[i].open;
  }
 
  toggleItem(i, j) {
    this.labReportsList[i].test[j].open = !this.labReportsList[i].test[j].open;
  }

  setColorOfList(index){
    index=index%4;
    if(index==0){
      return '#5e92f3';
    }
    if(index==1){
      return '#d4515d';
    }
    if(index==2){
      return '#5e92f3';
    }
    if(index==3){
      return '#d4515d';
    }
    
  }

  getReportClass(report){
    console.log('checking report',report);
    if(report.min===0&&report.max===0)
    return '';
    if(report.result>report.max||report.result<report.min)
    return 'red-report';
    else
    return 'green-report'
  }

  ionViewWillLeave() {
  //this.navCtrl.setRoot(TabContentPage);
  }
}
