import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { DoctorDetailsPage } from '../doctor-details/doctor-details';
import { CommonUtils } from '../../common/commonUtils';
import { TabContentPage } from '../tab-content/tab-content';

/**
 * Generated class for the DepartmentAndDoctorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-department-and-doctors',
  templateUrl: 'department-and-doctors.html'
})
export class DepartmentAndDoctorsPage {
  originalDoctorsList: any[];
  showDoctorsSearchUI: boolean=false;

  departsmentList: any[];
  doctorsList:any[]=[];
  public searchInput:string;
  public inValidDepartments:string[]=['PHYSICAL AND REHABILITATION MEDICINE','NURSING','RADIOLOGY','RHEUMATOLOGY','ANESTHESIA','NEUROLOGY','PULMONOLOGY','EMERGENCY ROOM','LABORATORY'];
  public tempDList;

  constructor(public commonUtills:CommonUtils,public navCtrl: NavController, public navParams: NavParams,public apiServiceProvider:ApiServiceProvider,public loadingCtrl: LoadingController) {
    console.log('ionViewDidLoad DepartmentAndDoctorsPage');

    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();

    this.apiServiceProvider.getDepartmentAndDoctorsList().subscribe((data)=>{
      loading.dismiss();
      console.log('dummy json data from the saved file ',data);
      this.departsmentList=data.departments;
      this.removeInvalidDepartments();
      console.log('test json data from the web file ',this.departsmentList);
      let docList= this.createDoctorsList();
      console.log('getting the filtered doc name ',docList);

    },(error)=>{
      console.log(error);
      loading.dismiss();
    });
    // this.apiServiceProvider.getDummyJsonDataFromSavedFile('dummyDoctorData.json').subscribe((data)=>{
    //   console.log('dummy json data from the saved file ',data);
    //   this.departsmentList=data.departments;
    // //  console.log('test json data from the web file ',this.departsmentList);
    //   this.removeInvalidDepartments();

    //   let docList= this.createDoctorsList();
    //   console.log('getting the filtered doc name ',docList);
    //   loading.dismiss();

    // });
  }

  removeInvalidDepartments(){
   // let tempDList=Object.create(this.departsmentList);
    this.inValidDepartments.forEach((invalidDepartment,i) => {
      let index = this.departsmentList.findIndex(p => p.name == invalidDepartment)
      if (index > -1) {
        console.log('going to remove the element on index ',index);
        this.departsmentList.splice(index, 1);
        console.log('length of new object is ',this.departsmentList.length,'and object is ',this.departsmentList);
    }
    });
    console.log('refined list of deparments',this.departsmentList);
  }


  ionViewDidLoad() {
  
  }

  //generating list of doctors for serch doctor functionality
  createDoctorsList(){
    this.departsmentList.forEach(department => {
      department.doctors.forEach(doctor => {
        doctor.departmentName=department.name;
        this.doctorsList.push(doctor);
      });
    }); 
    console.log('doctors array for searxhing is' ,this.doctorsList);
    this.originalDoctorsList=this.doctorsList;
  }

  toggleSection(i) {
    this.departsmentList[i].open = !this.departsmentList[i].open;
  }
 
  toggleItem(i, j) {
    this.departsmentList[i].children[j].open = !this.departsmentList[i].children[j].open;
  }

  //to toggle search box
  toggleSearchInputBox(){
    this.showDoctorsSearchUI=this.showDoctorsSearchUI===true?false:true;
    console.log(this.showDoctorsSearchUI);
  }

  //got doctorDetals page
  goTodoctorDetailsPage(doctorDetails){
    this.navCtrl.push(DoctorDetailsPage,{doctorDetails:doctorDetails});
  }
  setColorOfList(index){
    index=index%4;
    if(index==0){
      return '#5e92f3';
    }
    if(index==1){
      return '#d4515d';
    }
    if(index==2){
      return '#5e92f3';
    }
    if(index==3){
      return '#d4515d';
    }
    
  }
  
  // isDepartmentValid(departmentName:string){
  //   if (this.inValidDepartments.indexOf(departmentName) > -1) {
  //     return true;
  //   } else {
  //     //Not in the array
  //     return false;
  //    }
  // }
  ionViewWillLeave() {
    // console.log('seetin root pages');
    // this.navCtrl.setRoot(TabContentPage);
    }
}
