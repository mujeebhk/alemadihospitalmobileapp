import { Component } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DepartmentAndDoctorsPage } from "../department-and-doctors/department-and-doctors";
import { LapReportsPage } from "../lap-reports/lap-reports";
import { PharmacyListPage } from "../pharmacy/pharmacyList";
import { AlertController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CommonUtils } from '../../common/commonUtils';
import {
  FabContainer,
  LoadingController
} from "ionic-angular";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { AppointmentListPage } from '../appointment-list/appointment-list';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public userDetails:any;
  destinationAddress:any=[25.286106, 51.534817];

  fab: FabContainer;
  message = 'visit';
  alemadiAppInviteImageUrl = 'www/assets/images/logo.png';
  url = 'http://alemadihospital.com.qa/';
  fbAlemadiAppUrl = 'https://fb.me/1862369954026404';
  image = "'www/assets/images/logo.png";
  fbMessage = this.message + "  " + this.url;
  constructor(public commonUtills:CommonUtils,private nativePageTransitions: NativePageTransitions,public navCtrl: NavController, public navParams: NavParams,private socialSharing: SocialSharing,public alertCtrl: AlertController,private launchNavigator: LaunchNavigator) {
  }

  ionViewDidLoad() {
    this.userDetails=JSON.parse(sessionStorage.getItem('userDetails'));

    console.log('ionViewDidLoad HomePage');
  }

  goToDepartmentsAndDoctorPage(){
  
    this.navCtrl.push(DepartmentAndDoctorsPage);
  }
  goToLabReportsPage(){
    this.navCtrl.push(LapReportsPage);
  }

  goToPharmacyListPage(){
    this.navCtrl.push(PharmacyListPage);
  }
  goToAppointmentListPage(){
    this.navCtrl.push(AppointmentListPage);
  }

   //setting up the link to fab on click
   passFab(fab: FabContainer) {
    console.log('printing fab from inside passfab', fab);
    this.fab = fab;
  }
   //closing up the fab
   closeFab() {
      if (this.fab) {
        this.fab.close();
      }
   }
    shareOnfb(){
      this.socialSharing.shareViaFacebook(this.fbMessage,null,"http://alemadihospital.com.qa/").then(() => {
        // Success!
      }).catch(() => {
        console.log('twitter not installed');
        this.showAlert("You must have the Facebook app installed to share");
        
       // alert('fb not installed');
         // Error!
     });
   

    }

  shareOnTwitter() {
    this.fab.close();

    this.socialSharing.canShareVia('twitter',null,null,this.url).then((data)=>{
    this.socialSharing.shareViaTwitter(this.message, this.alemadiAppInviteImageUrl, this.url).then(() => {
      // Success!
    }).catch((error) => {
      console.log('user cancelled the operation ', error);
    });
    }).catch((error)=>{
      console.log('app might not be installed in tge device');
      this.showAlert("You must have the Twitter app installed to share");
    // alert('You must have the Twitter app installed to share');
    });

  }
  shareOnGoogle() {
   this.fab.close();
   this.socialSharing.canShareVia('Google+', this.fbMessage, null, this.alemadiAppInviteImageUrl, null).then(() => {

   this.socialSharing.shareVia('Google+', this.message, undefined, this.alemadiAppInviteImageUrl, this.url).then(() => {
     // Success!
   }).catch(() => {
     // Error!
       alert("err");
     });
     });

  }
  shareOnWhatsApp(){
    this.socialSharing.shareViaWhatsApp(this.message,null,this.url).then(() => {
      // Success!
    }).catch(() => {
      console.log('twitter not installed');
      this.showAlert("You must have the whatsapp app installed to share");
      //alert('WhatsApp not installed');
       // Error!
      });


  }
  showAlert(ErrorMessage) {
    let alert = this.alertCtrl.create({
     // title: 'New Friend!',
      subTitle:ErrorMessage ,
      buttons: ['OK']
    });
    alert.present();
  }
  navigateToHospital(){
   // alert("launch");
   let options: LaunchNavigatorOptions = {
  //  start: ', ON',
   // app: LaunchNavigator.APPS.UBER
  };
  
  this.launchNavigator.navigate('Al Hilal West, P.O. 5804, Doha, Qatar', options)
    .then(
      success => console.log('Launched navigator'),
      error => {console.log('Error launching navigator', error)
      this.showAlert("Unable to launch Navigator");
    }
    );
  }


  ionViewWillLeave() {}
   // this.commonUtills.slideAnimation('left');p
    
    //  let options: NativeTransitionOptions = {
    //    // direction: 'forward',
    //     duration: 500,
    //     slowdownfactor: 3,
    //     slidePixels: 20,
    //     iosdelay: 100,
    //     androiddelay: 150,
    //     fixedPixelsTop: 0,
    //     fixedPixelsBottom: 60
    //    };
    
    //  this.nativePageTransitions.slide(options);
    //   //  .then()
    //   //  .catch();
    
    // }
}

