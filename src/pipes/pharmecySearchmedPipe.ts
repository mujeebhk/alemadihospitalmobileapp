


import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchMeds'
})
export class PharmecySearchmedPipe implements PipeTransform {
    transform(items: any[], criteria: any): any {
                return items.filter(item =>{
                    if(criteria===null||criteria===undefined){
                        return true;
                    }
                   for (let key in item ) {
                     if((""+item[key]).toLowerCase().includes(criteria.toLowerCase())){
                        return true;
                     }
                   }
                   return false;
                });
            }
}