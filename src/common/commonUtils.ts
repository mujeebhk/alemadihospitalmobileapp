import { Injectable } from '@angular/core';
import { AlertController, Events, LoadingController, Platform, ToastController } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

@Injectable()
export class CommonUtils {


    

 public networkType: string = 'dummy';
 constructor( public platform: Platform,private nativePageTransitions: NativePageTransitions,
    public toastCtrl: ToastController, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public events: Events) {
    //constructor
 }
  
    showAlert(ErrorMessage) {
      let alert = this.alertCtrl.create({
       // title: 'New Friend!',
       subTitle:ErrorMessage ,
      buttons: ['OK']
      });
     alert.present();
   }

   slideAnimation(direction){
   let options: NativeTransitionOptions = {
        direction: direction,
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
       };

       this.nativePageTransitions.slide(options);
  }

  showLoadingAnimation(message: string): any {
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',

    });
    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 10000);
    return loading;
  }
}