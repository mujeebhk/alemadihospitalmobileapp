import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServiceProvider {
  public timeoutTime=10000;
  // public protocol='http://';
  // public endpoint='192.168.0.11';
  // public portNumber=':8000';

 //alemadi server configuration
  public protocol='http://';
  public endpoint='192.168.150.248';
  public portNumber=':8080'; 
  public urlEndpoint=this.protocol+this.endpoint+this.portNumber;
  

  constructor(public http: Http) {
    console.log('Hello ApiServiceProvider Provider');
  }


    //getting google user profile
    getGoogleUserProfile(userId){
      return this.http.get('getUrl')
      .map(res=>res.json());
    }
    

    //send otp using qatarID
    sendOTPusingQatarId(qatarID){
      return this.http
      .post(this.urlEndpoint+'/mobile/patient/sendOTP',{"QatarId": qatarID})
      .map(res=>res.json());
    }

    //verify otp and login
    verifyOtpAndLogin(patcode,otp){
      return this.http.post(this.urlEndpoint+"/mobile/patient/verifyOTP",{"PATCODE": patcode,
      "OTP": otp}).map(res=>res.json());
    }

    //get lab data from the server

    getLabDataUsingPatCode(patCode){
      return this.http.get(this.urlEndpoint+'/mobile/patient/labReport/patcode/'+patCode).map((res)=>res.json());
    }

    //get department and doctors details using
      getDepartmentAndDoctorsList(){
        return this.http.get(this.urlEndpoint+'/mobile/doctor/getDoctorsListByDepartment').map((res)=>res.json());
      }


      //get DateSlots For Doctor
      getSlotsDetailsForDoctor(date,clinicCode){
        return this.http.get(this.urlEndpoint+'/mobile/doctor/getDoctorsScheduleAndAppointments/clinicCode/'+clinicCode+'/date/'+date).map((res)=>res.json());
      }
      //get pharmacyLits
      getPharmacyList(){
        return this.http.get(this.urlEndpoint+'/mobile/pharmacy/getPharmacyList').map((res)=>res.json());
      }

    //get dummy json data from assests folder

    getDummyJsonDataFromSavedFile(fileName){
      return this.http.get('assets/'+fileName).map(res=>res.json());
    }
    saveAppointment(appointmentDetails){
      return this.http.post(this.urlEndpoint+'/mobile/patient/appointment',appointmentDetails).map(res=>res.json());
    }

     //get pharmacyLits
     getAppointmentList(patcode){
      return this.http.get(this.urlEndpoint+'/mobile/patient/getAppointmentsList/patcode/'+patcode).map((res)=>res.json());
    }

}
